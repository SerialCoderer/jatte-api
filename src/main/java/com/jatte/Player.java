package com.jatte;


import java.io.Serializable;
import java.util.UUID;


public class Player implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private int playerId;
    private final UUID clientUUID;
    private final String userName;
    private int score;
    private boolean ready = false;



    public Player(UUID clientUUID, String userName) throws IllegalArgumentException {

        if(clientUUID == null || (userName == null || userName.isEmpty())){
            throw new IllegalArgumentException("Invalid arguments for creating Player.");
        }

        this.clientUUID = clientUUID;
        this.userName = userName;
    }

    public final void setPlayerId(int initplayerId) {
        playerId = initplayerId;
    }





    public final int getPlayerId() {
        return playerId;
    }


    public final UUID getUUID() {
        return this.clientUUID;
    }


    public String getUserName() {
        return userName;
    }

    public final boolean getReadyVar() {
        return ready;
    }

    public final void setReadyVar(boolean intready) {
        ready = intready;
    }

    public final void addPlayerScore(int initScore) {
        score += initScore;
    }

    public final void setPlayerScore(int score) {
        this.score = score;
    }

    public final void resetPlayerScore() {
        score = 0;
    }

    public final int getPlayerScore() {
        return score;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        if (playerId != player.playerId) return false;
        if (clientUUID != null ? !clientUUID.equals(player.clientUUID) : player.clientUUID != null) return false;
        return userName != null ? userName.equals(player.userName) : player.userName == null;

    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + playerId;
        result = 31 * result + (clientUUID != null ? clientUUID.hashCode() : 0);
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        return result;
    }
}
