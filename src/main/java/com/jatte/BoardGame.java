package com.jatte;

/**
 * Game Description: this class is an abstract class the every Game
 * on the server must extent to run correct on the the server
 *
 * @author Jovaughn Lockridge jovaughn@jovacode.com Copyright
 */

import com.jatte.api.cards.Card;
import com.jatte.api.cards.Suits;
import com.jatte.client.GamePlayer;
import com.jatte.client.GameStateUpdate;
import com.jatte.service.GameStateObservable;

import com.jatte.spi.Game;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.*;

public abstract class BoardGame<T, P extends GamePlayer> implements Serializable, Game<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(BoardGame.class);

    private static final long serialVersionUID = 1L;
    private int roundsPlayed;
    private int numOfPlayersAllowed;
    private P whoMove;
    private boolean isFull = false;
    private Map<String, P> gamePlayers = new HashMap<>();
    private Map<Integer, P> gamePlayersByGameId = new HashMap<>();
    private Map<String, Integer> gamePlayerIds = new HashMap<>();
    private boolean isScoreUpdated = false;
    private String gameMessage = null;
    private String postGameMessage = null;
    private GameKey gameKey = null;
    private String gameStatus;
    private boolean isPostMoveUpdate;
    private transient GameStateObservable gameStateObservable;
    private Map<UUID, List<Card<Suits, Integer>>> playerCards;


    public BoardGame() {
    }

    public BoardGame(GameKey gameKey) {
        this.gameKey = gameKey;
    }

    public void setGameStateObservable(GameStateObservable gameStateObservable) {
        this.gameStateObservable = gameStateObservable;
    }

    /**
     *
     * Sets the number of players allowed to play in the Game
     * Object
     *
     * @param initnumOfPlayers: a int representation of the limit of players
     * allowed to play single Game Object instance
     *
     * <dt>precondition:: A GameObject object has been instantiated.
     *
     *
     */
    public final void setNumOfPlayersToPlay(int initnumOfPlayers) {
        numOfPlayersAllowed = initnumOfPlayers;
    }


    public void postMove(){}

    public abstract boolean isGameOver();

    /**
     *
     * Set the current move to a Client by gameId
     *
     * @param initPlayerMove: a int representation of the player to be set to
     * have the current move.
     * <dt>precondition:: A GameObject object has been instantiated .
     *
     */
    public final void setWhoMove(P initPlayerMove) {
        whoMove = initPlayerMove;
    }

    /**
     *
     * Returns an int representation of the Client that has the
     * ability to make a move in the Game Object
     *
     *
     */
    public final P getWhoMove() {
        return whoMove;
    }


    public abstract void addPlayer(String userName);

    protected void addPlayer(P player) {
        if (isFull == false) {
            int playerID = gamePlayersByGameId.size() + 1;
            gamePlayersByGameId.put(playerID, player);
            gamePlayers.put(player.getUserName(), player);
            gamePlayerIds.put(player.getUserName(), playerID);
            if (numOfPlayersAllowed == gamePlayers.size()) {
                isFull = true;
            }
        } else {
            LOGGER.info("Game is full");
        }
        LOGGER.info("{} players have joined gameId {} ", gamePlayersByGameId.size(), gameKey);
    }

    /**
     *
     * Returns an Hashmap of GamePlayers representation of all
     * the players contain in the Game Object instance
     *
     */
    public final Map<String, P> getGamePlayersMap() {
        return gamePlayers;
    }

    public P getGamePlayerByPlayerId(int playerID) {
        return gamePlayersByGameId.get(playerID);
    }


    public Integer getPlayerId(String userName) {
        return gamePlayerIds.get(userName);
    }

    /**
     *
     * Get player in game represented by UUID
     *
     * @param userName: returns the GamePlayer
     *
     *
     *
     */
    public final P getPlayer(String userName) {
        return gamePlayers.get(userName);
    }

    /**
     *
     * increments the rounds playerd in a Game Object
     *
     *
     */
    public void incrementRoundsPlayed() {

        roundsPlayed++;
    }

    /**
     *
     * Returns an int representation of the roundsPlayed in in
     * the Game Object
     *
     *
     */
    public int getRoundsPlayed() {
        return roundsPlayed;
    }

    /**
     *
     * Sets the roundsPlayed in a Game Object with a number
     *
     * @param initRoundsPlayed : a int representation of what roundsplayed will be set to.
     *
     *
     */
    public void setCurrentRoundNumber(int initRoundsPlayed) {
        roundsPlayed = initRoundsPlayed;
    }


    /**
     *
     * returns a boolean representation if the Game
     * is full or not
     *
     *
     */
    public abstract boolean isFull();


    public void setScoreUpdated(boolean isScoreUpdated) {
        this.isScoreUpdated = isScoreUpdated;
    }

    public boolean isScoreUpdated() {
        return this.isScoreUpdated;
    }

    public boolean isGameMessages() {
        return !(gameMessage == null || gameMessage.length() == 0);

    }

    public void setGameMessage(String message) {
        gameMessage = message;
    }

    public String getGameMessage() {
        return gameMessage;
    }

    public String getPostGameMessage() {
        return postGameMessage;
    }

    public GameKey getGameKey() {
        return gameKey;
    }

    public void setGameKey(GameKey gameKey) {
        this.gameKey = gameKey;
    }

    public Collection<P> getGamePlayers() {
        return gamePlayers.values();
    }

    public void setGameStatus(String gameStatus) {
        this.gameStatus = gameStatus;
    }

    public String getGameStatus() {
        return gameStatus;
    }


    public boolean isPostMoveUpdate() {
        return isPostMoveUpdate;
    }

    public void setIsPostMoveUpdate(boolean isPostMoveUpdate) {
        this.isPostMoveUpdate = isPostMoveUpdate;
    }

    public void notifyGameStatusChange() {
//        gameStateObservable.notify(this);
    }

    public abstract Map<String, Card<Suits, Integer>> getBoardTable ();

    public Map<UUID, List<Card<Suits, Integer>>> getPlayerCards() {
        return playerCards;
    }

    public void setPlayerCards(Map<UUID, List<Card<Suits, Integer>>> playerCards) {
        this.playerCards = playerCards;
    }

    public abstract GameStateUpdate move(PlayerMove playerMove);

    public abstract GameStateUpdate startGame();

    public abstract void endGame();

}

