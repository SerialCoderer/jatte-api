package com.jatte;

import com.jatte.api.cards.Card;
import com.jatte.api.cards.Suits;

import java.util.List;

public class PlayerMove {
    private String userName;
    private List<Card<Suits, Integer>> move;
    private String action;

    public PlayerMove(String action, String userName, List<Card<Suits, Integer>> move) {
        this.action = action;
        this.userName = userName;
        this.move = move;
    }

    public PlayerMove(String userName, List<Card<Suits, Integer>> move) {
        this("", userName, move);
    }

    public String getPlayer() {
        return userName;
    }

    public List<Card<Suits, Integer>> getMove() {
        return move;
    }
}
