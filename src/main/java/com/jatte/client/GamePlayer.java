package com.jatte.client;

import com.jatte.api.cards.Card;
import com.jatte.api.cards.Suits;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Jovaughn Lockridge on 9/16/16.
 */
public abstract class GamePlayer implements Serializable {

    private int score;
    private boolean ready = false;


    public GamePlayer(){
    }

    public final boolean getReadyVar() {
        return ready;
    }

    public final void setReadyVar(boolean intready) {
        ready = intready;
    }

    public final void addPlayerScore(int initScore) {
        score += initScore;
    }

    public final void setPlayerScore(int score) {
        this.score = score;
    }

    public final void resetPlayerScore() {
        score = 0;
    }

    public final int getPlayerScore() {
        return score;
    }

    public abstract String getUserName();

    public abstract List<Card<Suits, Integer>> getPlayerCards();

    public abstract Iterable<Card<Suits, Integer>> playerCards();

}
