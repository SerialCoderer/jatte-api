package com.jatte.client;

public interface GameDao {
	
	boolean login(String user, String password);
	boolean createUser(String user, String password);
}
