package com.jatte.client;



import com.jatte.api.cards.Card;
import com.jatte.api.cards.Suits;

import java.util.Map;
import java.util.UUID;

public class GameStateUpdate<P extends GamePlayer> {

    private Map<UUID, Card<Suits, Integer>> move;
    private Map<UUID, GamePlayer> players;
    private Map<String, P > playersByUserName;
    private String clientSideAction;

    public GameStateUpdate(String clientSideAction, Map<UUID, GamePlayer> players, Map<UUID, Card<Suits, Integer>> moves) {
        this.clientSideAction = clientSideAction;
        this.players = players;
        this.move = moves;
    }

    public GameStateUpdate(String clientSideAction, Map<String, P > playersByUserName) {
        this.clientSideAction = clientSideAction;
        this.playersByUserName = playersByUserName;
    }

    public Map<UUID, Card<Suits, Integer>> getMove() {
        return move;
    }

    public Map<UUID, GamePlayer> getPlayers() {
        return players;
    }

    public String getClientSideAction() {
        return clientSideAction;
    }

    public Map<String, P > getPlayersByUserName(){
        return playersByUserName;
    }

}
