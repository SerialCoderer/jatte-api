package com.jatte.client;

/**
 * Created by jovaughnlockridge1 on 5/31/16.
 */
public class ClientNetworkException extends Exception {

    public ClientNetworkException(String errorMessage){
        super(errorMessage);
    }

}
