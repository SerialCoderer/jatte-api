package com.jatte.client;


import java.util.UUID;

public interface IPlayer<T>{
    void updateBoard(T game);
    void updateScore(T score);
    void gameStateChange(T game);
    void message(String message);
    void startGame(T game);
    void successFullLogin(UUID uuid);
    void addGameActionListener(GameActionListener l);

}
