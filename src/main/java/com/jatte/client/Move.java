package com.jatte.client;

import java.io.Serializable;

/**
 * Created by Jovaughn Lockridge on 9/12/16.
 */
public class Move<T, R> implements Serializable{

    private T moveType;
    private R move;

    public Move(T moveType, R move){
        this.moveType = moveType;
        this.move = move;
    }

    public R getMove() {
        return move;
    }

    public T getMoveType() {
        return moveType;
    }

    @Override
    public String toString() {
        return "Move{" +
                "moveType=" + moveType +
                ", move=" + move +
                '}';
    }
}
