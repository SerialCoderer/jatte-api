package com.jatte;

import java.io.Serializable;

public final class GameKey implements Serializable {

    private final Integer gameKey;
    private final String game;

    public GameKey(Integer gameKey, String game) {
        this.gameKey = gameKey;
        this.game = game;
    }

    public Integer getGameKey() {
        return gameKey;
    }

    public String getGame() {
        return game;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameKey gameKey1 = (GameKey) o;
        if (gameKey != null ? !gameKey.equals(gameKey1.gameKey) : gameKey1.gameKey != null) return false;
        return !(game != null ? !game.equals(gameKey1.game) : gameKey1.game != null);
    }

    @Override
    public int hashCode() {
        int result = gameKey != null ? gameKey.hashCode() : 0;
        result = 31 * result + (game != null ? game.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "GameKey{" +
                "gameKey=" + gameKey +
                ", game='" + game + '\'' +
                '}';
    }
}
