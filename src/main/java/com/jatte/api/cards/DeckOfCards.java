package com.jatte.api.cards;

/**
 * *******************************************************************
 * Class: DeckOfCards Description: this class extends an ArrayList creates a 52
 * deck of cards of all the card suits
 *
 * @author Jovaughn Lockridge jovaughn@jovaapps.com Copyright
 * @ 2006
 * <p/>
 * ******************************************************************
 */

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Random;

public class DeckOfCards implements Serializable, Cloneable {

    private Card[] deck = new Card[52];
    private int pointer = 0;
    private final int FULL = 52;


    /**
     * It intializes a  DeckOfCards object which consist of all distinct Diamonds, Clubs, Hearts,
     * Spades suit cards
     * <p/>
     */
    public DeckOfCards() {

        initDeck();
    }

    /**
     * Removes a card from the deck and returns the  card
     * <p/>
     * <dt>precondition: A DeckOfCards object has been instantiated.
     *
     * @returns: A Cards representation of card is return.
     */
    public Card getCard() throws EmptyDeckException {

        if (pointer == FULL) {
            throw new EmptyDeckException("Deck is empty");
        }

        Card card = deck[pointer];
        pointer++;
        return card;
    }


    /**
     * Checks if the deck of cards is empty.
     *
     * @returns
     */
    public boolean isEmpty() {
        return pointer == FULL;
    }


    /**
     *
     */
    public void initDeck() {

        LinkedList<Card> deck = new LinkedList<>();
        deck.add(new Card(Suits.CLUBS, new Integer(2), "2"));
        deck.add(new Card(Suits.CLUBS, new Integer(3), "3"));
        deck.add(new Card(Suits.CLUBS, new Integer(4), "4"));
        deck.add(new Card(Suits.CLUBS, new Integer(5), "5"));
        deck.add(new Card(Suits.CLUBS, new Integer(6), "6"));
        deck.add(new Card(Suits.CLUBS, new Integer(7), "7"));
        deck.add(new Card(Suits.CLUBS, new Integer(8), "8"));
        deck.add(new Card(Suits.CLUBS, new Integer(9), "9"));
        deck.add(new Card(Suits.CLUBS, new Integer(10), "10"));
        deck.add(new Card(Suits.CLUBS, new Integer(11), "Jack"));
        deck.add(new Card(Suits.CLUBS, new Integer(12), "Queen"));
        deck.add(new Card(Suits.CLUBS, new Integer(13), "King"));
        deck.add(new Card(Suits.CLUBS, new Integer(14), "Ace"));

        deck.add(new Card(Suits.HEARTS, new Integer(2), "2"));
        deck.add(new Card(Suits.HEARTS, new Integer(3), "3"));
        deck.add(new Card(Suits.HEARTS, new Integer(4), "4"));
        deck.add(new Card(Suits.HEARTS, new Integer(5), "5"));
        deck.add(new Card(Suits.HEARTS, new Integer(6), "6"));
        deck.add(new Card(Suits.HEARTS, new Integer(7), "7"));
        deck.add(new Card(Suits.HEARTS, new Integer(8), "8"));
        deck.add(new Card(Suits.HEARTS, new Integer(9), "9"));
        deck.add(new Card(Suits.HEARTS, new Integer(10), "10"));
        deck.add(new Card(Suits.HEARTS, new Integer(11), "Jack"));
        deck.add(new Card(Suits.HEARTS, new Integer(12), "Queen"));
        deck.add(new Card(Suits.HEARTS, new Integer(13), "King"));
        deck.add(new Card(Suits.HEARTS, new Integer(14), "Ace"));

        deck.add(new Card(Suits.DIAMONDS, new Integer(2), "2"));
        deck.add(new Card(Suits.DIAMONDS, new Integer(3), "3"));
        deck.add(new Card(Suits.DIAMONDS, new Integer(4), "4"));
        deck.add(new Card(Suits.DIAMONDS, new Integer(5), "5"));
        deck.add(new Card(Suits.DIAMONDS, new Integer(6), "6"));
        deck.add(new Card(Suits.DIAMONDS, new Integer(7), "7"));
        deck.add(new Card(Suits.DIAMONDS, new Integer(8), "8"));
        deck.add(new Card(Suits.DIAMONDS, new Integer(9), "9"));
        deck.add(new Card(Suits.DIAMONDS, new Integer(10), "10"));
        deck.add(new Card(Suits.DIAMONDS, new Integer(11), "Jack"));
        deck.add(new Card(Suits.DIAMONDS, new Integer(12), "Queen"));
        deck.add(new Card(Suits.DIAMONDS, new Integer(13), "King"));
        deck.add(new Card(Suits.DIAMONDS, new Integer(14), "Ace"));

        deck.add(new Card(Suits.SPADES, new Integer(2), "2"));
        deck.add(new Card(Suits.SPADES, new Integer(3), "3"));
        deck.add(new Card(Suits.SPADES, new Integer(4), "4"));
        deck.add(new Card(Suits.SPADES, new Integer(5), "5"));
        deck.add(new Card(Suits.SPADES, new Integer(6), "6"));
        deck.add(new Card(Suits.SPADES, new Integer(7), "7"));
        deck.add(new Card(Suits.SPADES, new Integer(8), "8"));
        deck.add(new Card(Suits.SPADES, new Integer(9), "9"));
        deck.add(new Card(Suits.SPADES, new Integer(10), "10"));
        deck.add(new Card(Suits.SPADES, new Integer(11), "Jack"));
        deck.add(new Card(Suits.SPADES, new Integer(12), "Queen"));
        deck.add(new Card(Suits.SPADES, new Integer(13), "King"));
        deck.add(new Card(Suits.SPADES, new Integer(14), "Ace"));

        boolean usedVals[] = new boolean[52];

        while (deck.size() > 0) {

            Random r = new Random();
            int index = r.nextInt(52);

            if (usedVals[index] == false) {
                this.deck[index] = deck.removeFirst();
                usedVals[index] = true;
            }

        }
    }

}
