package com.jatte.api.cards;

/**
 * Created by jovaughnlockridge1 on 3/24/16.
 */
public enum Suits {

    HEARTS, CLUBS, DIAMONDS, SPADES

}
