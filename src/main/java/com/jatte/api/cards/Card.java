package com.jatte.api.cards;
/*********************************************************************
 * Class:    Cards
 * Description: this class is a generic Card with no suit
 *
 * Created By Jovaughn Lockridge
 *
 * Copyright @ 2006
 *
 ********************************************************************/

import java.io.Serializable;
import java.util.Objects;


public class Card<T, U extends Number>  implements Serializable, Cloneable, Comparable<Card> {

	private final T suit;
	private final U cardValue;
	private final String face;
	private U ordinal;

	public Card(T suit, U cardValue, String face){
		this(suit, cardValue, face, cardValue);
	}


	/**
	 * This constructor creates a card with a ordinal value irrespective
	 * of the carValue;
	 *
	 * @param suit
	 * @param cardValue
	 * @param face
	 * @param ordinal
	 */
	public Card(T suit, U cardValue, String face, U ordinal) {
		this.suit = suit;
		this.cardValue = cardValue;
		this.face = face;
		this.ordinal = ordinal;
	}


	public U getOrdinal(){
		return ordinal;
	}


	public U getCardValue(){
		return cardValue;
	}

	public String getFace(){
		return face;
	}
	
	/**
	* The defined suit of the card instance.
	* 
	* @precondition:
	*    This Cards object has been instantiated.
	*
	* @return:
	*    a string value of this object Suit
	*/
	public T getSuit(){
		return suit;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Card<?, ?> card = (Card<?, ?>) o;
		return Objects.equals(suit, card.suit) &&
			Objects.equals(cardValue, card.cardValue) &&
			Objects.equals(face, card.face);
	}

	@Override
	public int hashCode() {
		return Objects.hash(suit, cardValue, face);
	}

	/**
	 * Creates a String representation of a Cards object.
	 * <p/>
	 * <dt>precondition:
	 * This Cards object been instantiated.
	 *
	 * @returns: A String representation of the Cards object.
	 **/
	@Override
	public String toString() {
		return "Cards{" +
				"suit=" + suit +
				", cardValue=" + cardValue +
				", face="+ face +
				'}';
	}

	///TODO: Hack for now will fix later
	public String getFilename(){
		StringBuilder sb = new StringBuilder();
		if(suit==null){
			sb.append(face).append(".gif");
		}else {
			sb.append(face).append(suit.toString()).append(".gif");
		}
		return sb.toString().toLowerCase();
	}


	@Override
	public int compareTo(Card o) {

		if(this.ordinal.equals(o.ordinal)) {
			return 0;
		}

		if(this.ordinal.longValue() > o.ordinal.longValue()){
			return 1;
		}

		return -1;

	}
}
