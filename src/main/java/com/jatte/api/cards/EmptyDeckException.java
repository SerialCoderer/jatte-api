package com.jatte.api.cards;

/**
 * Created by jovaughnlockridge1 on 5/31/16.
 */
public class EmptyDeckException extends Exception {


    public EmptyDeckException(String errorMessage){
        super(errorMessage);
    }

}
